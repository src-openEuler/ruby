%global ruby_version 3.2.7

# Bundled libraries versions
%global rubygems_version 3.4.19
%global rubygems_molinillo_version 0.8.0

%global bundler_version 2.4.19
%global bigdecimal_version 3.1.3
%global did_you_mean_version 1.6.3
%global erb_version 4.0.2
%global irb_version 1.6.2
%global racc_version 1.6.2
%global io_console_version 0.6.0
%global json_version 2.6.3
%global openssl_version 3.1.0
%global psych_version 5.0.1
%global rdoc_version 6.5.1.1
%global minitest_version 5.25.1
%global power_assert_version 2.0.3
%global rake_version 13.0.6
%global rbs_version 2.8.2
%global test_unit_version 3.5.7
%global rexml_version 3.3.9
%global rss_version 0.3.1
%global syntax_suggest_version 1.1.0
%global typeprof_version 0.21.3
%global net_ftp_version 0.2.1
%global net_imap_version 0.3.8
%global net_pop_version 0.1.2
%global net_smtp_version 0.3.4
%global matrix_version 0.4.2
%global prime_version 0.1.2
%global debug_version 1.7.1

Name:      ruby
Version:   %{ruby_version}
Release:   151
Summary:   Object-oriented scripting language interpreter
License:   (Ruby OR BSD-2-Clause) AND (Ruby OR BSD-2-Clause OR GPL-1.0-or-later) AND BSD-3-Clause AND (GPL-3.0-or-later WITH Bison-exception-2.2) AND ISC AND Public Domain AND MIT AND CC0 AND zlib AND Unicode-DFS-2015
URL:       https://www.ruby-lang.org/en/

Source0:   https://cache.ruby-lang.org/pub/ruby/3.1/%{name}-%{version}.tar.xz
Source1:   operating_system.rb
Source2:   libruby.stp
Source3:   ruby-exercise.stp
Source4:   macros.ruby
Source5:   macros.rubygems
Source6:   abrt_prelude.rb
Source8:   rubygems.attr
Source9:   rubygems.req
Source10:  rubygems.prov
Source11:  rubygems.con
Source12:  test_abrt.rb
Source13:  test_systemtap.rb

%{load:%{SOURCE4}}
%{load:%{SOURCE5}}

# Separated source for security updates
Source6001: https://rubygems.org/downloads/net-imap-%{net_imap_version}.gem

# Fix ruby_version abuse.
# https://bugs.ruby-lang.org/issues/11002
Patch0: ruby-2.3.0-ruby_version.patch
# http://bugs.ruby-lang.org/issues/7807
Patch1: ruby-2.1.0-Prevent-duplicated-paths-when-empty-version-string-i.patch
# Allows to override libruby.so placement. Hopefully we will be able to return
# to plain --with-rubyarchprefix.
# http://bugs.ruby-lang.org/issues/8973
Patch2: ruby-2.1.0-Enable-configuration-of-archlibdir.patch
# Force multiarch directories for i.86 to be always named i386. This solves
# some differencies in build between Fedora and RHEL.
Patch3: ruby-2.1.0-always-use-i386.patch
# Allows to install RubyGems into custom directory, outside of Ruby's tree.
# http://bugs.ruby-lang.org/issues/5617
Patch4: ruby-2.1.0-custom-rubygems-location.patch
# The ABRT hook used to be initialized by preludes via following patches:
# https://bugs.ruby-lang.org/issues/8566
# https://bugs.ruby-lang.org/issues/15306
# Unfortunately, due to https://bugs.ruby-lang.org/issues/16254
# and especially since https://github.com/ruby/ruby/pull/2735
# this would require boostrapping:
# https://lists.fedoraproject.org/archives/list/ruby-sig@lists.fedoraproject.org/message/LH6L6YJOYQT4Y5ZNOO4SLIPTUWZ5V45Q/
# For now, load the ABRT hook via this simple patch:
Patch6: ruby-2.7.0-Initialize-ABRT-hook.patch
# Avoid possible timeout errors in TestBugReporter#test_bug_reporter_add.
# https://bugs.ruby-lang.org/issues/16492
Patch19: ruby-2.7.1-Timeout-the-test_bug_reporter_add-witout-raising-err.patch

Patch6003: backport-CVE-2019-19204.patch
Patch6004: backport-CVE-2019-19246.patch
Patch6005: backport-CVE-2019-16161.patch
Patch6006: backport-CVE-2019-16162.patch
Patch6007: backport-CVE-2019-16163.patch
Patch6018: backport-rubygems-rubygems-Drop-to-support-Psych-3.0-bundled-.patch
Patch6019: backport-0001-CVE-2024-35221.patch
Patch6020: backport-0002-CVE-2024-35221.patch
Patch6021: backport-0003-CVE-2024-35221.patch
Patch6022: backport-0004-CVE-2024-35221.patch
Patch6023: backport-0005-CVE-2024-35221.patch
Patch6028: backport-CVE-2024-47220.patch
Patch6029: backport-CVE-2025-27219.patch
Patch6030: backport-CVE-2025-27220.patch
Patch6031: backport-0001-CVE-2025-27221.patch
Patch6032: backport-0002-CVE-2025-27221.patch

Provides:  %{name}-libs = %{version}-%{release}
Obsoletes: %{name}-libs < %{version}-%{release}

Provides:  ruby(runtime_executable) = %{version} ruby(release) = %{version} bundled(ccan-build_assert)
Provides:  bundled(ccan-check_type) bundled(ccan-container_of) bundled(ccan-list)
Obsoletes: ruby-tcltk < 2.4.0

# The Net::Telnet and XMLRPC were removed. https://bugs.ruby-lang.org/issues/16484
Obsoletes: rubygem-net-telnet < 0.1.1-%{release}
Obsoletes: rubygem-xmlrpc < 0.3.0-%{release}

Suggests:   rubypick
Recommends: ruby(rubygems) >= %{rubygems_version} rubygem(bigdecimal) >= %{bigdecimal_version}
Recommends: rubygem(did_you_mean) >= 1.2.0 rubygem(openssl) >= %{openssl_version}
BuildRequires: autoconf gdbm-devel gmp-devel libffi-devel openssl-devel libyaml-devel readline-devel
BuildRequires: procps git gcc systemtap-sdt-devel cmake perl elfutils-extra
BuildRequires: rubygem-rdoc zlib-devel

%description
Ruby is a fast and easy interpreted scripting language for object-oriented programming.
It has many functions for processing text Files and perform system management tasks (such as Perl).

%package devel
Summary:    Ruby development environment
Requires:   %{name} = %{version}-%{release}
Requires:   rubygems

%description devel
Headers and libraries for building extension libraries for extensions Ruby or Ruby embedded applications.

%package -n rubygems
Summary:    Ruby standard for wrapping ruby libraries
Version:    %{rubygems_version}
License:    (Ruby OR MIT) AND BSD-2-Clause AND (BSD-2-Clause OR Ruby) AND (Ruby OR BSD-2-Clause OR GPL-1.0-or-later) AND MIT
Requires:   ruby(release) rubygem(openssl) >= 2.1.0 rubygem(psych) >= %{psych_version} 
Recommends: rubygem(rdoc) >= %{rdoc_version} rubygem(io-console) >= %{io_console_version}
Provides:   gem = %{version}-%{release} ruby(rubygems) = %{version}-%{release} bundled(rubygem-molinillo) = %{rubygems_molinillo_version}
BuildArch:  noarch

%description -n rubygems
The Ruby standard for publishing and managing third party libraries provided by RubyGems.

%package -n rubygems-devel
Summary:    For packaging RubyGems
Version:    %{rubygems_version}
License:    MIT
Requires:   ruby(rubygems) = %{version}-%{release} rubygem(json) >= %{json_version} rubygem(rdoc) >= %{rdoc_version}
BuildArch:  noarch

%description -n rubygems-devel
Provide macros and development tools for packaging RubyGems.

%package -n rubygem-rake
Summary:    make-like utility base on ruby
Version:    %{rake_version}
License:    MIT
Requires:   ruby(release) ruby(rubygems) >= %{rubygems_version}
Provides:   rake = %{version}-%{release} rubygem(rake) = %{version}-%{release}
BuildArch:  noarch

%description -n rubygem-rake
Rake is a Make-like program implemented in Ruby,Tasks and dependencies are specified in standard Ruby syntax.

%package -n rubygem-rbs
Summary:    Type signature for Ruby
Version:    %{rbs_version}
License:    Ruby OR BSD-2-Clause
Requires:   ruby(release)
Requires:   ruby(rubygems) >= %{rubygems_version}
Provides:   rubygem(rbs) = %{version}-%{release}

%description -n rubygem-rbs
RBS is the language for type signatures for Ruby and standard library
definitions.

%package irb
Summary:    The Interactive Ruby
Version:    %{ruby_version}
License:    Ruby OR BSD-2-Clause
Requires:   %{name}-libs = %{ruby_version}
Provides:   irb = %{version}-%{release} ruby(irb) = %{version}-%{release}
BuildArch:  noarch

%description irb
The irb is acronym for Interactive Ruby,It evaluates ruby expression from the terminal.

%package -n rubygem-rdoc
Summary:    Generate HTML and command-line documentation for Ruby projects
Version:    %{rdoc_version}
License:    GPL-2.0-only AND Ruby AND BSD-3-Clause AND CC-BY-2.5 AND OFL-1.1-RFN
Requires:   ruby(release) ruby(rubygems) >= %{rubygems_version} ruby(irb) = %{ruby_version} rubygem(io-console) >= %{io_console_version} rubygem(json) >= %{json_version}
Provides:   rdoc = %{version}-%{release} ri = %{version}-%{release} rubygem(rdoc) = %{version}-%{release}
BuildArch:  noarch

%description -n rubygem-rdoc
RDoc generates HTML and command line documentation for Ruby projects.,RDoc contains "rdoc" and "ri" tools for generating
and displaying online documentation.

%package help
Summary:    Documentation for ruby
Requires:   rubygem-rdoc
Provides:   %{name}-doc = %{version}-%{release}
Obsoletes:  %{name}-doc < %{version}-%{release}
BuildArch:  noarch

%description help
This package provides documentation for ruby.

%package -n rubygem-bigdecimal
Summary:    Provide arbitrary-precision floating point decimal arithmetic
Version:    %{bigdecimal_version}
License:    Ruby OR BSD-2-Clause
Requires:   ruby(release) ruby(rubygems) >= %{rubygems_version}
Provides:   rubygem(bigdecimal) = %{version}-%{release}

%description -n rubygem-bigdecimal
BigDecimal provides similar support for very large or very accurate floating point numbers.

%package -n rubygem-did_you_mean
Summary:    "Did you mean?" experience in Ruby
Version:    %{did_you_mean_version}
License:    MIT
Requires:   ruby(release) ruby(rubygems) >= %{rubygems_version}
Provides:   rubygem(did_you_mean) = %{version}-%{release}
BuildArch:  noarch

%description -n rubygem-did_you_mean
The error message will tell you the right one when you misspelled something.

%package -n rubygem-io-console
Summary:    Simple console utilizing library
Version:    %{io_console_version}
Requires:   ruby(release) ruby(rubygems) >= %{rubygems_version}
Provides:   rubygem(io-console) = %{version}-%{release}

%description -n rubygem-io-console
IO / Console provides very simple and portable access to the console. It does not provide higher-level functions
such as curses and readline.

%package -n rubygem-json
Summary:    JSON implementation as a Ruby extension in C
Version:    %{json_version}
License:    (Ruby OR BSD-2-Clause) AND Unicode-DFS-2015
Requires:   ruby(release) ruby(rubygems) >= %{rubygems_version}
Provides:   rubygem(json) = %{version}-%{release}

%description -n rubygem-json
According to RFC 4627,this package implements the JSON specification.

%package -n rubygem-minitest
Summary:    Provide complete testing facilities
Version:    %{minitest_version}
License:    MIT
Requires:   ruby(release) ruby(rubygems) >= %{rubygems_version}
Provides:   rubygem(minitest) = %{version}-%{release}
BuildArch:  noarch

%description -n rubygem-minitest
minitest/unit - Unit testing framework.
minitest/spec - Full-featured spec engine.
minitest/benchmark - Assert the performance of algorithms in a repeatable manner.
minitest/mock - Tiny mock object framework.
minitest/pride - Show pride in the test and add color to the test output.

%package -n rubygem-openssl
Summary:    Provide SSL、TLS and general purpose cryptography
Version:    %{openssl_version}
License:    Ruby or BSD-2-Clause
Requires:   ruby(release) ruby(rubygems) >= %{rubygems_version}
Provides:   rubygem(openssl) = %{version}-%{release}

%description -n rubygem-openssl
This package provides SSL、TLS and general purpose cryptography.

%package -n rubygem-psych
Summary:    Ruby's libyaml wrapper
Version:    %{psych_version}
License:    MIT
Requires:   ruby(release) ruby(rubygems) >= %{rubygems_version}
Provides:   rubygem(psych) = %{version}-%{release}

%description -n rubygem-psych
Psych is a YAML parser and emitter. According to wrapping libyaml, Psych knows how to
serialize and de-serialize most Ruby objects to and from the YAML format.

%package -n rubygem-test-unit
Summary:    Unit testing framework for Ruby
Version:    %{test_unit_version}
License:    (Ruby OR BSD-2-Clause) AND (Ruby OR BSD-2-Clause)
Requires:   ruby(release) ruby(rubygems) >= %{rubygems_version} rubygem(power_assert)
Provides:   rubygem(test-unit) = %{version}-%{release}
BuildArch:  noarch

%description -n rubygem-test-unit
Test::Unit (test-unit) is unit testing framework for Ruby based on xUnit principles. writing tests, checking results
and automated testing are provided in Ruby.

%package -n rubygem-rexml
Summary:    An XML toolkit for Ruby
Version:    %{rexml_version}
License:    BSD-2-Clause
URL:        https://github.com/ruby/rexml
Requires:   ruby(release)
Requires:   ruby(rubygems) >= %{rubygems_version}
Provides:   rubygem(rexml) = %{version}-%{release}
BuildArch:  noarch

%description -n rubygem-rexml
REXML was inspired by the Electric XML library for Java, which features an
easy-to-use API, small size, and speed. Hopefully, REXML, designed with the same
philosophy, has these same features. I've tried to keep the API as intuitive as
possible, and have followed the Ruby methodology for method naming and code
flow, rather than mirroring the Java API.

REXML supports both tree and stream document parsing. Stream parsing is faster
(about 1.5 times as fast). However, with stream parsing, you don't get access to
features such as XPath.

%package -n rubygem-rss
Summary:    Family of libraries that support various formats of XML "feeds"
Version:    %{rss_version}
License:    BSD-2-Clause
URL:        https://github.com/ruby/rss
Requires:   ruby(release)
Requires:   ruby(rubygems) >= %{rubygems_version}
Provides:   rubygem(rss) = %{version}-%{release}
BuildArch:  noarch

%description -n rubygem-rss
Really Simple Syndication (RSS) is a family of formats that describe 'feeds',
specially constructed XML documents that allow an interested person to subscribe
and receive updates from a particular web service. This library provides tooling
to read and create these feeds.

%package -n rubygem-typeprof
Summary:    TypeProf is a type analysis tool for Ruby code based on abstract interpretation
Version:    %{typeprof_version}
License:    MIT
URL:        https://github.com/ruby/typeprof
Requires:   ruby(release)
Requires:   ruby(rubygems) >= %{rubygems_version}
Requires:   rubygem(rbs) >= %{rbs_version}
Provides:   rubygem(typeprof) = %{version}-%{release}
BuildArch:  noarch

%description -n rubygem-typeprof
TypeProf performs a type analysis of non-annotated Ruby code.
It abstractly executes input Ruby code in a level of types instead of values,
gathers what types are passed to and returned by methods, and prints the
analysis result in RBS format, a standard type description format for Ruby
3.0.

%package bundled-gems
Summary:    Bundled gems which are part of Ruby StdLib
Requires:   ruby(rubygems) >= %{rubygems_version}
# Runtime dependency of rubygem(debug).
Recommends: rubygem(irb) >= %{irb_version}
Provides:   rubygem(net-ftp) = %{net_ftp_version}
Provides:   rubygem(net-imap) = %{net_imap_version}
Provides:   rubygem(net-pop) = %{net_pop_version}
Provides:   rubygem(net-smtp) = %{net_smtp_version}
Provides:   rubygem(matrix) = %{matrix_version}
Provides:   rubygem(prime) = %{prime_version}
Provides:   rubygem(debug) = %{debug_version}

%description bundled-gems
Bundled gems which are part of Ruby StdLib. While being part of Ruby, these
needs to be listed in Gemfile to be used by Bundler.

%prep
%autosetup -n ruby-%{ruby_version} -p1

rm -rf ext/psych/yaml
rm -rf ext/fiddle/libffi*

cp -a %{SOURCE3} .

# Update gems by replace it with downloaded gem
(
rm -f gems/net-imap*.gem
cp %{S:6001} gems/net-imap-%{net_imap_version}.gem
sed -i -e 's,net-imap        0.3.4.1,net-imap           %{net_imap_version},' gems/bundled_gems
)

# test gems version earlier
[ -f gems/debug-%{debug_version}.gem ]
[ -f gems/matrix-%{matrix_version}.gem ]
[ -f gems/minitest-%{minitest_version}.gem ]
[ -f gems/net-ftp-%{net_ftp_version}.gem ]
[ -f gems/net-imap-%{net_imap_version}.gem ]
[ -f gems/net-pop-%{net_pop_version}.gem ]
[ -f gems/net-smtp-%{net_smtp_version}.gem ]
[ -f gems/power_assert-%{power_assert_version}.gem ]
[ -f gems/prime-%{prime_version}.gem ]
[ -f gems/rake-%{rake_version}.gem ]
[ -f gems/rbs-%{rbs_version}.gem ]
[ -f gems/rexml-%{rexml_version}.gem ]
[ -f gems/rss-%{rss_version}.gem ]
[ -f gems/test-unit-%{test_unit_version}.gem ]
[ -f gems/typeprof-%{typeprof_version}.gem ]

%build
autoconf

%configure --with-rubylibprefix='%{ruby_libdir}' --with-archlibdir='%{_libdir}' --with-rubyarchprefix='%{ruby_libarchdir}' \
        --with-sitedir='%{ruby_sitelibdir}' --with-sitearchdir='%{ruby_sitearchdir}' --with-vendordir='%{ruby_vendorlibdir}' \
        --with-vendorarchdir='%{ruby_vendorarchdir}' --with-rubyhdrdir='%{_includedir}' -with-rubyarchhdrdir='%{_includedir}' \
        --with-sitearchhdrdir='$(sitehdrdir)/$(arch)' --with-vendorarchhdrdir='$(vendorhdrdir)/$(arch)' \
        --with-rubygemsdir='%{_datadir}/rubygems' --with-ruby-pc='%{name}.pc' --with-compress-debug-sections=no --disable-rpath \
        --enable-shared --with-ruby-version='' --enable-multiarch  --enable-mkmf-verbose \


%make_build COPY="cp -p" Q=

%install
%make_install

sed -i 's/Version: \${ruby_version}/Version: %{ruby_version}/' %{buildroot}%{_libdir}/pkgconfig/%{name}.pc

# Kill bundled certificates, as they should be part of ca-certificates.
for cert in \
  rubygems.org/GlobalSignRootCA.pem \
  rubygems.org/GlobalSignRootCA_R3.pem
do
  rm %{buildroot}%{_datadir}/rubygems/rubygems/ssl_certs/$cert
  rm -d $(dirname %{buildroot}%{_datadir}/rubygems/rubygems/ssl_certs/$cert) || :
done

test ! "$(ls -A  %{buildroot}%{_datadir}/rubygems/rubygems/ssl_certs/ 2>/dev/null)"

install -d %{buildroot}%{_rpmconfigdir}/macros.d
install -m 644 %{SOURCE4} %{buildroot}%{_rpmconfigdir}/macros.d/macros.ruby
install -m 644 %{SOURCE5} %{buildroot}%{_rpmconfigdir}/macros.d/macros.rubygems
sed -i "s/%%{name}/%{name}/" %{buildroot}%{_rpmconfigdir}/macros.d/macros.ruby
sed -i "s/%%{name}/%{name}/" %{buildroot}%{_rpmconfigdir}/macros.d/macros.rubygems

install -d %{buildroot}%{_rpmconfigdir}/fileattrs
install -m 644 %{SOURCE8} %{buildroot}%{_rpmconfigdir}/fileattrs
install -m 755 %{SOURCE9} %{buildroot}%{_rpmconfigdir}
install -m 755 %{SOURCE10} %{buildroot}%{_rpmconfigdir}
install -m 755 %{SOURCE11} %{buildroot}%{_rpmconfigdir}

install -d %{buildroot}%{_datadir}/rubygems/rubygems/defaults
cp %{SOURCE1} %{buildroot}%{_datadir}/rubygems/rubygems/defaults
if [ -d %{buildroot}%{ruby_libdir}/gems ]; then
  mv %{buildroot}%{ruby_libdir}/gems %{buildroot}%{gem_dir}
fi

install -d %{buildroot}%{_exec_prefix}/lib{,64}/gems/%{name}
install -d %{buildroot}%{gem_dir}/gems/rdoc-%{rdoc_version}/lib

mv %{buildroot}%{ruby_libdir}/rdoc* %{buildroot}%{gem_dir}/gems/rdoc-%{rdoc_version}/lib
mv %{buildroot}%{gem_dir}/specifications/default/rdoc-%{rdoc_version}.gemspec %{buildroot}%{gem_dir}/specifications

mkdir -p %{buildroot}%{gem_dir}/gems/bigdecimal-%{bigdecimal_version}/lib
mkdir -p %{buildroot}%{_libdir}/gems/%{name}/bigdecimal-%{bigdecimal_version}/bigdecimal
mv %{buildroot}%{ruby_libdir}/bigdecimal %{buildroot}%{gem_dir}/gems/bigdecimal-%{bigdecimal_version}/lib
mv %{buildroot}%{ruby_libarchdir}/bigdecimal.so %{buildroot}%{_libdir}/gems/%{name}/bigdecimal-%{bigdecimal_version}
touch %{buildroot}%{_libdir}/gems/%{name}/bigdecimal-%{bigdecimal_version}/gem.build_complete
mv %{buildroot}%{gem_dir}/specifications/default/bigdecimal-%{bigdecimal_version}.gemspec %{buildroot}%{gem_dir}/specifications
ln -s %{gem_dir}/gems/bigdecimal-%{bigdecimal_version}/lib/bigdecimal %{buildroot}%{ruby_libdir}/bigdecimal
ln -s %{_libdir}/gems/%{name}/bigdecimal-%{bigdecimal_version}/bigdecimal.so %{buildroot}%{ruby_libarchdir}/bigdecimal.so

mkdir -p %{buildroot}%{gem_dir}/gems/io-console-%{io_console_version}/lib
mkdir -p %{buildroot}%{_libdir}/gems/%{name}/io-console-%{io_console_version}/io
mv %{buildroot}%{ruby_libdir}/io %{buildroot}%{gem_dir}/gems/io-console-%{io_console_version}/lib
mv %{buildroot}%{ruby_libarchdir}/io/console.so %{buildroot}%{_libdir}/gems/%{name}/io-console-%{io_console_version}/io
touch %{buildroot}%{_libdir}/gems/%{name}/io-console-%{io_console_version}/gem.build_complete
mv %{buildroot}%{gem_dir}/specifications/default/io-console-%{io_console_version}.gemspec %{buildroot}%{gem_dir}/specifications
ln -s %{gem_dir}/gems/io-console-%{io_console_version}/lib/io %{buildroot}%{ruby_libdir}/io
ln -s %{_libdir}/gems/%{name}/io-console-%{io_console_version}/io/console.so %{buildroot}%{ruby_libarchdir}/io/console.so

install -d %{buildroot}%{gem_dir}/gems/json-%{json_version}/lib
install -d %{buildroot}%{_libdir}/gems/%{name}/json-%{json_version}
mv %{buildroot}%{ruby_libdir}/json* %{buildroot}%{gem_dir}/gems/json-%{json_version}/lib
mv %{buildroot}%{ruby_libarchdir}/json/ %{buildroot}%{_libdir}/gems/%{name}/json-%{json_version}/
touch %{buildroot}%{_libdir}/gems/%{name}/json-%{json_version}/gem.build_complete
mv %{buildroot}%{gem_dir}/specifications/default/json-%{json_version}.gemspec %{buildroot}%{gem_dir}/specifications
ln -s %{gem_dir}/gems/json-%{json_version}/lib/json.rb %{buildroot}%{ruby_libdir}/json.rb
ln -s %{gem_dir}/gems/json-%{json_version}/lib/json %{buildroot}%{ruby_libdir}/json
ln -s %{_libdir}/gems/%{name}/json-%{json_version}/json/ %{buildroot}%{ruby_libarchdir}/json

mkdir -p %{buildroot}%{gem_dir}/gems/openssl-%{openssl_version}/lib
mkdir -p %{buildroot}%{_libdir}/gems/%{name}/openssl-%{openssl_version}
mv %{buildroot}%{ruby_libdir}/openssl* %{buildroot}%{gem_dir}/gems/openssl-%{openssl_version}/lib
mv %{buildroot}%{ruby_libarchdir}/openssl.so %{buildroot}%{_libdir}/gems/%{name}/openssl-%{openssl_version}/
touch %{buildroot}%{_libdir}/gems/%{name}/openssl-%{openssl_version}/gem.build_complete
mv %{buildroot}%{gem_dir}/specifications/default/openssl-%{openssl_version}.gemspec %{buildroot}%{gem_dir}/specifications

install -d %{buildroot}%{ruby_libdir}/openssl
find %{buildroot}%{gem_dir}/gems/openssl-%{openssl_version}/lib/openssl -maxdepth 1 -type f -exec \
  sh -c 'ln -s %{gem_dir}/gems/openssl-%{openssl_version}/lib/openssl/`basename {}` %{buildroot}%{ruby_libdir}/openssl' \;
ln -s %{gem_dir}/gems/openssl-%{openssl_version}/lib/openssl.rb %{buildroot}%{ruby_libdir}/openssl.rb
ln -s %{_libdir}/gems/%{name}/openssl-%{openssl_version}/openssl.so %{buildroot}%{ruby_libarchdir}/openssl.so

mkdir -p %{buildroot}%{gem_dir}/gems/psych-%{psych_version}/lib
mkdir -p %{buildroot}%{_libdir}/gems/%{name}/psych-%{psych_version}
mv %{buildroot}%{ruby_libdir}/psych* %{buildroot}%{gem_dir}/gems/psych-%{psych_version}/lib
mv %{buildroot}%{ruby_libarchdir}/psych.so %{buildroot}%{_libdir}/gems/%{name}/psych-%{psych_version}/
touch %{buildroot}%{_libdir}/gems/%{name}/psych-%{psych_version}/gem.build_complete
mv %{buildroot}%{gem_dir}/specifications/default/psych-%{psych_version}.gemspec %{buildroot}%{gem_dir}/specifications
ln -s %{gem_dir}/gems/psych-%{psych_version}/lib/psych %{buildroot}%{ruby_libdir}/psych
ln -s %{gem_dir}/gems/psych-%{psych_version}/lib/psych.rb %{buildroot}%{ruby_libdir}/psych.rb
ln -s %{_libdir}/gems/%{name}/psych-%{psych_version}/psych.so %{buildroot}%{ruby_libarchdir}/psych.so

find %{buildroot}%{gem_dir}/extensions/*-%{_target_os}/%{version}/* -maxdepth 0 \
  -exec mv '{}' %{buildroot}%{_libdir}/gems/%{name}/ \; || echo "No gem binary extensions to move."

# bundler is built as standalone pacakge
rm -fr %{buildroot}{%{_bindir}/bundle,%{_bindir}/bundler,%{gem_dir}/gems/bundler-%{bundler_version},%{ruby_libdir}/bundler}

sed -i '/^end$/ i\
  s.extensions = ["json/ext/parser.so", "json/ext/generator.so"]' %{buildroot}%{gem_dir}/specifications/json-%{json_version}.gemspec

mv %{buildroot}%{gem_dir}/gems/rake-%{rake_version}/doc/rake.1 %{buildroot}%{_mandir}/man1

install -d %{buildroot}%{_datadir}/systemtap/tapset
sed -e "s|@LIBRARY_PATH@|%(echo %{_libdir} | sed 's/64//')*/libruby.so.3.0|" \
  %{SOURCE2} > %{buildroot}%{_datadir}/systemtap/tapset/libruby.so.3.0.stp

sed -i -r "s|( \*.*\*)\/(.*)|\1\\\/\2|" %{buildroot}%{_datadir}/systemtap/tapset/libruby.so.3.0.stp

find doc -maxdepth 1 -type f ! -name '.*' ! -name '*.ja*' > .ruby-doc.en
echo 'doc/images' >> .ruby-doc.en
echo 'doc/syntax' >> .ruby-doc.en

find doc -maxdepth 1 -type f -name '*.ja*' > .ruby-doc.ja
echo 'doc/irb' >> .ruby-doc.ja
echo 'doc/pty' >> .ruby-doc.ja

sed -i 's/^/%doc /' .ruby-doc.*
sed -i 's/^/%lang(ja) /' .ruby-doc.ja


%check

[ "`make runruby TESTRUN_SCRIPT='bin/gem -v' | tail -1`" == '%{rubygems_version}' ]

[ "`make runruby TESTRUN_SCRIPT=\"-e \\\" module Gem; module Resolver; end; end; \
  require 'rubygems/resolver/molinillo/lib/molinillo/gem_metadata'; \
  puts Gem::Resolver::Molinillo::VERSION\\\"\" | tail -1`" == '%{rubygems_molinillo_version}' ]

touch abrt.rb

make runruby TESTRUN_SCRIPT="--enable-gems %{SOURCE12}"
make runruby TESTRUN_SCRIPT=%{SOURCE13}

%files
%license BSDL COPYING GPL LEGAL
%doc README.md 
%lang(ja) %license COPYING.ja

%{_bindir}/{erb,ruby,syntax_suggest}

%dir %{ruby_vendorlibdir}
%dir %{ruby_vendorarchdir}
%dir %{ruby_libdir}
%{ruby_libdir}/{*.rb,cgi,digest,drb,fiddle,forwardable,matrix,net,optparse,racc,rexml}
%{ruby_libdir}/{rinda,ripper,rss,shell,syslog,unicode_normalize,uri,webrick,yaml}

%{_libdir}/libruby.so.*
%dir %{ruby_libarchdir}

%dir %{ruby_libarchdir}/digest
%{ruby_libarchdir}/digest/{bubblebabble.so,md5.so,rmd160.so,sha1.so,sha2.so}

%dir %{ruby_libarchdir}/enc
%{ruby_libarchdir}/enc/*.so

%dir %{ruby_libarchdir}/enc/trans
%{ruby_libarchdir}/enc/trans/*.so

%dir %{ruby_libarchdir}/cgi
%{ruby_libarchdir}/cgi/escape.so

%dir %{ruby_libarchdir}/io
%{ruby_libarchdir}/io/{nonblock.so,wait.so}

%dir %{ruby_libarchdir}/racc
%{ruby_libarchdir}/racc/cparse.so

%dir %{ruby_libarchdir}/rbconfig
%{ruby_libarchdir}/rbconfig.rb
%{ruby_libarchdir}/rbconfig/sizeof.so

%{ruby_libarchdir}/erb/escape.so
%{ruby_libarchdir}/{continuation.so,coverage.so,date_core.so,dbm.so,monitor.so}
%{ruby_libarchdir}/{etc.so,fcntl.so,fiber.so,fiddle.so,gdbm.so,digest.so,nkf.so,objspace.so,pathname.so,pty.so}
%{ruby_libarchdir}/{readline.so,ripper.so,sdbm.so,socket.so,stringio.so,strscan.so,syslog.so,zlib.so}

%{_datadir}/systemtap

%exclude %{ruby_sitelibdir}
%exclude %{ruby_sitearchdir}
%exclude %{ruby_libdir}/irb.rb
%exclude %{ruby_libdir}/json.rb
%exclude %{ruby_libdir}/openssl.rb
%exclude %{ruby_libdir}/psych.rb
%exclude %{ruby_libdir}/irb
# https://bugs.ruby-lang.org/issue/19298
%exclude %{ruby_libdir}/mjit

%dir %{ruby_libdir}/ruby_vm
%{ruby_libdir}/ruby_vm/mjit
%exclude %{_bindir}/racc
%{gem_dir}/gems/erb-%{erb_version}/libexec/erb
%{gem_dir}/gems/irb-%{irb_version}/exe/irb
%{gem_dir}/gems/syntax_suggest-%{syntax_suggest_version}
%exclude %{gem_dir}/gems/racc-%{racc_version}/bin
%{ruby_libdir}/benchmark/version.rb
%{ruby_libdir}/csv/core_ext/array.rb
%{ruby_libdir}/csv/core_ext/string.rb
%{ruby_libdir}/csv/delete_suffix.rb
%{ruby_libdir}/csv/fields_converter.rb
%{ruby_libdir}/csv/match_p.rb
%{ruby_libdir}/csv/parser.rb
%{ruby_libdir}/csv/row.rb
%{ruby_libdir}/csv/table.rb
%{ruby_libdir}/csv/version.rb
%{ruby_libdir}/csv/writer.rb
%{ruby_libdir}/csv/input_record_separator.rb
%{ruby_libdir}/logger/errors.rb
%{ruby_libdir}/logger/formatter.rb
%{ruby_libdir}/logger/log_device.rb
%{ruby_libdir}/logger/period.rb
%{ruby_libdir}/logger/severity.rb
%{ruby_libdir}/logger/version.rb
%{ruby_libdir}/reline/ansi.rb
%{ruby_libdir}/reline/config.rb
%{ruby_libdir}/reline/general_io.rb
%{ruby_libdir}/reline/history.rb
%{ruby_libdir}/reline/key_actor.rb
%{ruby_libdir}/reline/key_actor/base.rb
%{ruby_libdir}/reline/key_actor/emacs.rb
%{ruby_libdir}/reline/key_actor/vi_command.rb
%{ruby_libdir}/reline/key_actor/vi_insert.rb
%{ruby_libdir}/reline/key_stroke.rb
%{ruby_libdir}/reline/kill_ring.rb
%{ruby_libdir}/reline/line_editor.rb
%{ruby_libdir}/reline/unicode.rb
%{ruby_libdir}/reline/unicode/east_asian_width.rb
%{ruby_libdir}/reline/version.rb
%{ruby_libdir}/reline/windows.rb
%{ruby_libdir}/set/sorted_set.rb
%{ruby_libdir}/syntax_suggest*
%{ruby_libdir}/erb*
%{ruby_libdir}/objspace/trace.rb
%{ruby_libdir}/open3/version.rb
%{ruby_libdir}/random/formatter.rb
%{ruby_libdir}/reline/terminfo.rb
%{ruby_libdir}/error_highlight/base.rb
%{ruby_libdir}/error_highlight/core_ext.rb
%{ruby_libdir}/error_highlight/formatter.rb
%{ruby_libdir}/error_highlight/version.rb
%dir %{gem_dir}/gems/prime-%{prime_version}
%{gem_dir}/gems/prime-%{prime_version}/Gemfile
%license %{gem_dir}/gems/prime-%{prime_version}/LICENSE.txt
%doc %{gem_dir}/gems/prime-%{prime_version}/README.md
%{gem_dir}/gems/prime-%{prime_version}/Rakefile
%{gem_dir}/gems/prime-%{prime_version}/bin
%{gem_dir}/gems/prime-%{prime_version}/lib
%{gem_dir}/specifications/prime-%{prime_version}.gemspec

%files devel
%license BSDL COPYING GPL LEGAL
%lang(ja) %license COPYING.ja

%{_rpmconfigdir}/macros.d/macros.ruby

%{_includedir}/*
%{_libdir}/libruby.so
%{_libdir}/pkgconfig/%{name}.pc

%files -n rubygems
%{_bindir}/gem
%dir %{_datadir}/rubygems
%{_datadir}/rubygems/rubygems
%{_datadir}/rubygems/rubygems.rb

%dir %{gem_dir}
%dir %{gem_dir}/build_info
%dir %{gem_dir}/cache
%dir %{gem_dir}/doc
%dir %{gem_dir}/extensions
%dir %{gem_dir}/gems
%dir %{gem_dir}/specifications
%dir %{gem_dir}/specifications/default
%dir %{_exec_prefix}/lib*/gems
%dir %{_exec_prefix}/lib*/gems/ruby

%exclude %{gem_dir}/cache/*

%{gem_dir}/specifications/default/*

%files -n rubygems-devel
%{_rpmconfigdir}/macros.d/macros.rubygems
%{_rpmconfigdir}/fileattrs/rubygems.attr
%{_rpmconfigdir}/{rubygems.req,rubygems.prov,rubygems.con}

%files -n rubygem-rake
%{_bindir}/rake
%{gem_dir}/gems/rake-%{rake_version}
%{gem_dir}/specifications/rake-%{rake_version}.gemspec

%files -n rubygem-rbs
%{_bindir}/rbs
%dir %{_libdir}/gems/%{name}/rbs-%{rbs_version}
%{_libdir}/gems/%{name}/rbs-%{rbs_version}/gem.build_complete
%{_libdir}/gems/%{name}/rbs-%{rbs_version}/rbs_extension.so
%dir %{gem_dir}/gems/rbs-%{rbs_version}
%exclude %{gem_dir}/gems/rbs-%{rbs_version}/.*
%license %{gem_dir}/gems/rbs-%{rbs_version}/BSDL
%doc %{gem_dir}/gems/rbs-%{rbs_version}/CHANGELOG.md
%license %{gem_dir}/gems/rbs-%{rbs_version}/COPYING
%{gem_dir}/gems/rbs-%{rbs_version}/Gemfile
%doc %{gem_dir}/gems/rbs-%{rbs_version}/README.md
%{gem_dir}/gems/rbs-%{rbs_version}/Rakefile
%{gem_dir}/gems/rbs-%{rbs_version}/Steepfile
%{gem_dir}/gems/rbs-%{rbs_version}/core
%doc %{gem_dir}/gems/rbs-%{rbs_version}/docs
%{gem_dir}/gems/rbs-%{rbs_version}/exe
%{gem_dir}/gems/rbs-%{rbs_version}/goodcheck.yml
%{gem_dir}/gems/rbs-%{rbs_version}/lib
%{gem_dir}/gems/rbs-%{rbs_version}/schema
%{gem_dir}/gems/rbs-%{rbs_version}/sig
%{gem_dir}/gems/rbs-%{rbs_version}/stdlib
%{gem_dir}/gems/rbs-%{rbs_version}/steep
%{gem_dir}/gems/rbs-%{rbs_version}/Gemfile.lock
%{gem_dir}/gems/rbs-%{rbs_version}/ext/
%{gem_dir}/gems/rbs-%{rbs_version}/exts.mk
%{gem_dir}/specifications/rbs-%{rbs_version}.gemspec

%files irb
%{_bindir}/irb
%{ruby_libdir}/irb*

%files -n rubygem-rdoc
%{_bindir}/{rdoc,ri}
%{gem_dir}/gems/rdoc-%{rdoc_version}
%{gem_dir}/specifications/rdoc-%{rdoc_version}.gemspec

%files help -f .ruby-doc.en -f .ruby-doc.ja
%doc README.md ChangeLog ruby-exercise.stp
%{_datadir}/ri
%{_mandir}/man1/ri*
%{_mandir}/man1/erb*
%{_mandir}/man1/irb.1*
%{_mandir}/man1/rake.1*
%{_mandir}/man1/ruby*

%files -n rubygem-bigdecimal
%{ruby_libdir}/bigdecimal
%{ruby_libarchdir}/bigdecimal.so*
%{_libdir}/gems/%{name}/bigdecimal-%{bigdecimal_version}
%{gem_dir}/gems/bigdecimal-%{bigdecimal_version}
%{gem_dir}/specifications/bigdecimal-%{bigdecimal_version}.gemspec

%files -n rubygem-did_you_mean
%{ruby_libdir}/did_you_mean/core_ext/name_error.rb
%{ruby_libdir}/did_you_mean/experimental.rb
%{ruby_libdir}/did_you_mean/formatters/plain_formatter.rb
%{ruby_libdir}/did_you_mean/formatters/verbose_formatter.rb
%{ruby_libdir}/did_you_mean/jaro_winkler.rb
%{ruby_libdir}/did_you_mean/levenshtein.rb
%{ruby_libdir}/did_you_mean/spell_checker.rb
%{ruby_libdir}/did_you_mean/spell_checkers/key_error_checker.rb
%{ruby_libdir}/did_you_mean/spell_checkers/method_name_checker.rb
%{ruby_libdir}/did_you_mean/spell_checkers/name_error_checkers.rb
%{ruby_libdir}/did_you_mean/spell_checkers/name_error_checkers/class_name_checker.rb
%{ruby_libdir}/did_you_mean/spell_checkers/name_error_checkers/variable_name_checker.rb
%{ruby_libdir}/did_you_mean/spell_checkers/null_checker.rb
%{ruby_libdir}/did_you_mean/spell_checkers/require_path_checker.rb
%{ruby_libdir}/did_you_mean/tree_spell_checker.rb
%{ruby_libdir}/did_you_mean/verbose.rb
%{ruby_libdir}/did_you_mean/version.rb
%{ruby_libdir}/did_you_mean/formatter.rb
%{ruby_libdir}/did_you_mean/spell_checkers/pattern_key_name_checker.rb

%files -n rubygem-io-console
%{ruby_libdir}/io
%{ruby_libarchdir}/io/console.so
%{_libdir}/gems/%{name}/io-console-%{io_console_version}
%{gem_dir}/gems/io-console-%{io_console_version}
%{gem_dir}/specifications/io-console-%{io_console_version}.gemspec

%files -n rubygem-json
%{ruby_libdir}/json*
%{ruby_libarchdir}/json*
%{_libdir}/gems/%{name}/json-%{json_version}
%{gem_dir}/gems/json-%{json_version}
%{gem_dir}/specifications/json-%{json_version}.gemspec

%files -n rubygem-minitest
%{gem_dir}/gems/minitest-%{minitest_version}
%{gem_dir}/specifications/minitest-%{minitest_version}.gemspec
%exclude %{gem_dir}/gems/minitest-%{minitest_version}/.*

%files -n rubygem-openssl
%{ruby_libdir}/openssl
%{ruby_libdir}/openssl.rb
%{ruby_libarchdir}/openssl.so
%{_libdir}/gems/%{name}/openssl-%{openssl_version}
%{gem_dir}/gems/openssl-%{openssl_version}
%{gem_dir}/specifications/openssl-%{openssl_version}.gemspec

%exclude %{gem_dir}/gems/power_assert-%{power_assert_version}
%exclude %{gem_dir}/specifications/power_assert-%{power_assert_version}.gemspec

%files -n rubygem-psych
%{ruby_libdir}/psych
%{ruby_libdir}/psych.rb
%{ruby_libarchdir}/psych.so
%{_libdir}/gems/%{name}/psych-%{psych_version}
%{gem_dir}/gems/psych-%{psych_version}
%{gem_dir}/specifications/psych-%{psych_version}.gemspec

%files -n rubygem-test-unit
%{gem_dir}/gems/test-unit-%{test_unit_version}
%{gem_dir}/specifications/test-unit-%{test_unit_version}.gemspec

%files -n rubygem-rexml
%dir %{gem_dir}/gems/rexml-%{rexml_version}
%license %{gem_dir}/gems/rexml-%{rexml_version}/LICENSE.txt
%doc %{gem_dir}/gems/rexml-%{rexml_version}/NEWS.md
%doc %{gem_dir}/gems/rexml-%{rexml_version}/doc
%{gem_dir}/gems/rexml-%{rexml_version}/lib
%{gem_dir}/specifications/rexml-%{rexml_version}.gemspec
%doc %{gem_dir}/gems/rexml-%{rexml_version}/README.md

%files -n rubygem-rss
%dir %{gem_dir}/gems/rss-%{rss_version}
%exclude %{gem_dir}/gems/rss-%{rss_version}/.*
%license %{gem_dir}/gems/rss-%{rss_version}/LICENSE.txt
%doc %{gem_dir}/gems/rss-%{rss_version}/NEWS.md
%{gem_dir}/gems/rss-%{rss_version}/lib
%{gem_dir}/specifications/rss-%{rss_version}.gemspec
%doc %{gem_dir}/gems/rss-%{rss_version}/README.md

%files -n rubygem-typeprof
%dir %{gem_dir}/gems/typeprof-%{typeprof_version}
%{_bindir}/typeprof
%exclude %{gem_dir}/gems/typeprof-%{typeprof_version}/.*
%license %{gem_dir}/gems/typeprof-%{typeprof_version}/LICENSE
%{gem_dir}/gems/typeprof-%{typeprof_version}/exe
%{gem_dir}/gems/typeprof-%{typeprof_version}/lib
%doc %{gem_dir}/gems/typeprof-%{typeprof_version}/tools
%{gem_dir}/specifications/typeprof-%{typeprof_version}.gemspec
%doc %{gem_dir}/gems/typeprof-%{typeprof_version}/Gemfile*
%doc %{gem_dir}/gems/typeprof-%{typeprof_version}/README.md
%doc %{gem_dir}/gems/typeprof-%{typeprof_version}/Rakefile
%doc %{gem_dir}/gems/typeprof-%{typeprof_version}/typeprof-lsp

%files bundled-gems
%{_bindir}/rdbg
%dir %{_libdir}/gems/%{name}/debug-%{debug_version}
%{_libdir}/gems/%{name}/debug-%{debug_version}/gem.build_complete
%dir %{_libdir}/gems/%{name}/debug-%{debug_version}/debug
%{_libdir}/gems/%{name}/debug-%{debug_version}/debug/debug.so
%dir %{gem_dir}/gems/debug-%{debug_version}
%exclude %{gem_dir}/gems/debug-%{debug_version}/.*
%doc %{gem_dir}/gems/debug-%{debug_version}/CONTRIBUTING.md
%{gem_dir}/gems/debug-%{debug_version}/Gemfile
%license %{gem_dir}/gems/debug-%{debug_version}/LICENSE.txt
%doc %{gem_dir}/gems/debug-%{debug_version}/README.md
%{gem_dir}/gems/debug-%{debug_version}/Rakefile
%doc %{gem_dir}/gems/debug-%{debug_version}/TODO.md
%{gem_dir}/gems/debug-%{debug_version}/exe
%{gem_dir}/gems/debug-%{debug_version}/lib
%{gem_dir}/gems/debug-%{debug_version}/misc
%{gem_dir}/gems/debug-%{debug_version}/ext/
%{gem_dir}/gems/debug-%{debug_version}/exts.mk
%{gem_dir}/specifications/debug-%{debug_version}.gemspec

%dir %{gem_dir}/gems/net-ftp-%{net_ftp_version}
%{gem_dir}/gems/net-ftp-%{net_ftp_version}/Gemfile
%license %{gem_dir}/gems/net-ftp-%{net_ftp_version}/LICENSE.txt
%doc %{gem_dir}/gems/net-ftp-%{net_ftp_version}/README.md
%{gem_dir}/gems/net-ftp-%{net_ftp_version}/Rakefile
%{gem_dir}/gems/net-ftp-%{net_ftp_version}/lib
%{gem_dir}/specifications/net-ftp-%{net_ftp_version}.gemspec

%dir %{gem_dir}/gems/net-imap-%{net_imap_version}
%{gem_dir}/gems/net-imap-%{net_imap_version}/Gemfile
%license %{gem_dir}/gems/net-imap-%{net_imap_version}/LICENSE.txt
%doc %{gem_dir}/gems/net-imap-%{net_imap_version}/README.md
%{gem_dir}/gems/net-imap-%{net_imap_version}/Rakefile
%doc %{gem_dir}/gems/net-imap-%{net_imap_version}/docs
%{gem_dir}/gems/net-imap-%{net_imap_version}/benchmarks
%{gem_dir}/gems/net-imap-%{net_imap_version}/lib
%{gem_dir}/gems/net-imap-%{net_imap_version}/rakelib
%{gem_dir}/specifications/net-imap-%{net_imap_version}.gemspec

%dir %{gem_dir}/gems/net-pop-%{net_pop_version}
%{gem_dir}/gems/net-pop-%{net_pop_version}/Gemfile
%license %{gem_dir}/gems/net-pop-%{net_pop_version}/LICENSE.txt
%doc %{gem_dir}/gems/net-pop-%{net_pop_version}/README.md
%{gem_dir}/gems/net-pop-%{net_pop_version}/Rakefile
%{gem_dir}/gems/net-pop-%{net_pop_version}/lib
%{gem_dir}/specifications/net-pop-%{net_pop_version}.gemspec

%dir %{gem_dir}/gems/net-smtp-%{net_smtp_version}
%license %{gem_dir}/gems/net-smtp-%{net_smtp_version}/LICENSE.txt
%{gem_dir}/gems/net-smtp-%{net_smtp_version}/lib
%{gem_dir}/specifications/net-smtp-%{net_smtp_version}.gemspec

%dir %{gem_dir}/gems/matrix-%{matrix_version}
%license %{gem_dir}/gems/matrix-%{matrix_version}/LICENSE.txt
%{gem_dir}/gems/matrix-%{matrix_version}/lib
%{gem_dir}/specifications/matrix-%{matrix_version}.gemspec

%changelog
* Fri Feb 28 2025 shixuantong <shixuantong1@huawei.com> - 3.2.7-151
- fix CVE-2025-27219 CVE-2025-27220 CVE-2025-27221

* Thu Feb 13 2025 Funda Wang <fundawang@yeah.net> - 3.2.7-150
- update to 3.2.7
- update bundled net-imap to 0.3.8 to fix CVE-2025-25186

* Thu Nov 07 2024 Funda Wang <fundawang@yeah.net> - 3.2.6-149
- update to 3.2.6

* Tue Oct 29 2024 Funda Wang <fundawang@yeah.net> - 3.2.5-148
- update rexml to 3.3.9
- fix CVE-2024-35176, CVE-2024-41946, CVE-2024-39908
  CVE-2024-41123, CVE-2024-43398, CVE-2024-49761

* Tue Oct 15 2024 Funda Wang <fundawang@yeah.net> - 3.2.5-147
- update to 3.2.5

* Tue Oct 08 2024 shixuantong <shixuantong1@huawei.com> - 3.2.2-146
- fix CVE-2024-47220

* Thu Aug 29 2024 shixuantong <shixuantong1@huawei.com> - 3.2.2-145
- upgrade rexml to fix CVE-2024-39908 CVE-2024-41123 CVE-2024-43398

* Wed Aug 14 2024 wangjiang <wangjiang37@h-partners.com> - 3.2.2-144
- License compliance rectification

* Thu Aug 08 2024 zhangxianting <zhangxianting@uniontech.com> - 3.2.2-143
- fix CVE-2024-41946

* Sat Jul 06 2024 shixuantong <shixuantong1@huawei.com> - 3.2.2-142
- upgrade rexml to fix CVE-2024-35176

* Tue Jun 18 2024 shixuantong <shixuantong1@huawei.com> - 3.2.2-141
- fix CVE-2024-35221

* Mon May 6 2024 zhoupengcheng <zhoupengcheng11@huawei.com> - 3.2.2-140
- fix CVE-2024-27282

* Tue Mar 26 2024 shixuantong <shixuantong1@huawei.com> - 3.2.2-139
- fix CVE-2024-27281

* Mon Sep 11 2023 shixuantong <shixuantong1@huawei.com> - 3.2.2-138
- remove old so file

* Fri Jul 28 2023 shixuantong <shixuantong1@huawei.com> - 3.2.2-137
- upgrade version to 3.2.2

* Sat Jul 08 2023 shixuantong <shixuantong1@huawei.com> - 3.1.3-136
- fix CVE-2023-36617

* Fri Jun 02 2023 shixuantong <shixuantong1@huawei.com> - 3.1.3-135
- remove rubygem-power_assert

* Wed Apr 12 2023 shixuantong <shixuantong1@huawei.com> - 3.1.3-134
- fix CVE-2023-28755 CVE-2023-28756

* Mon Mar 20 2023 shixuantong <shixuantong1@huawei.com> - 3.1.3-133
- Make 'io/console/size' as optional dependency

* Mon Mar 13 2023 shixuantong <shixuantong1@huawei.com> - 3.1.3-132
- ruby/irb Drop hard dependency on RDoc

* Fri Mar 10 2023 shixuantong <shixuantong1@huawei.com> - 3.1.3-131
- remove rubygem-bundler

* Sat Mar 04 2023 shixuantong <shixuantong1@huawei.com> - 3.1.3-130
- move the prime component to the main package 

* Sat Feb 04 2023 shixuantong <shixuantong1@huawei.com> - 3.1.3-129
- remove old libruby.so file

* Sat Feb 04 2023 shixuantong <shixuantong1@huawei.com> - 3.1.3-128
- add zlib-devel to buildrequires 

* Thu Dec 29 2022 shixuantong <shixuantong1@huawei.com> - 3.1.3-127
- upgrade version to 3.1.3

* Thu Sep 22 2022 shixuantong <shixuantong1@huawei.com> - 3.0.3-126
- remove duplicated bigdecimal.rb file

* Thu Sep 22 2022 shixuantong <shixuantong1@huawei.com> - 3.0.3-125
- fix build failure caused by command eu-readelf not found

* Fri Jul 01 2022 wangjiang <wangjiang37@h-partners.com> - 3.0.3-124
- fix CVE-2019-19204 CVE-2019-19246 CVE-2019-16161 CVE-2019-16162 CVE-2019-16163

* Sun May 29 2022 ExtinctFire <shenyining_00@126.com> - 3.0.3-123
- fix CVE-2022-28738 CVE-2022-28739

* Sat May 21 2022 shixuantong <shixuantong@h-partners.com> - 3.0.3-122
- drop dependency on ruby-help
 
* Wed Mar 02 2022 tianwei12 <tianwei12@h-partners.com> - 3.0.3-121
- fix conflict with rubygem-racc

* Wed Mar 02 2022 tianwei12 <tianwei12@h-partners.com> - 3.0.3-120
- fix rubygem-rspec-support rubygem-open4 build failed

* Wed Feb 23 2022 shixuantong <shixuantong@h-partners.com> - 3.0.3-119
- Add tests for `--template-stylesheets` option

* Tue Feb 08 2022 shixuantong <shixuantong@h-partners.com> - 3.0.3-118
- change release number for rebuild

* Tue Feb 08 2022 shangyibin <shangyibin1@h-partners.com> - 3.0.3-3
- Old version of libruby.so was kept for compatibility,remove it.

* Thu Jan 06 2022 shangyibin <shangyibin1@huawei.com> - 3.0.3-2
- add libruby.so.2.5 and libruby.so.2.5.8 file

* Fri Dec 31 2021 shangyibin <shangyibin1@huawei.com> - 3.0.3-1
- Upgrade to version 3.0.3

* Sat Jul 31 2021 shixuantong <shixuantong@huawei.com> - 2.5.8-114
- fix CVE-2021-31799 CVE-2021-31810 CVE-2021-32066

* Fri Apr 30 2021 Shinwell_Hu <huxinwei@huawei.com> - 2.5.8-113
- Upgrade bundled REXML gem to fix CVE-2021-28965, which is an XML
  round-trip vulnerability in REXML.

* Tue Apr 20 2021 shixuantong <shixuantong@huawei.com> - 2.5.8-112
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:change release number for rebuild

* Thu Nov 5 2020 wutao <wutao61@huawei.com> - 2.5.8-4
- fix CVE-2020-25613
- WEBrick,a simple HTTP server bundled with Ruby,had not
- checked the transfer-encoding header value rigorously.
- An attacker may potentially exploit this issue to bypass
- a reverse proxy,which may lead to an HTTP Request Smuggling
- attack.

* Fri Aug 7 2020 shixuantong <shixuantong@huawei.com> - 2.5.8-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix rdoc and ri command error problem

* Tue Aug 4 2020 shixuantong <shixuantong@huawei.com> - 2.5.8-2
- Type:NA
- ID:NA
- SUG:NA
- DESC:change package irb version

* Mon Jul 27 2020 shixuantong <shixuantong@huawei.com> - 2.5.8-1
- Type:NA
- ID:NA
- SUG:NA
- DESC:update to 2.5.8

*Wed Jul 08 2020 zhangjiapeng <zhangjiapeng9@huawei.com> - 2.5.1-107
- Type:N/A
- ID:N/A
- SUG:N/A
- DESC:modify patch information in spec file

* Mon Jun 22 2020 zhanghua <zhanghua40@huawei.com> - 2.5.1-106
- Type:cves
- ID:CVE-2020-10663
- SUG:restart
- DESC:fix CVE-2020-10663

* Thu May 07 2020 huanghaitao <huanghaitao@huawei.com> - 2.5.1-105
- Type:cves
- ID:CVE-2020-10933
- SUG:restart
- DESC:fix CVE-2020-10933

* Mon Feb 03 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.5.1-104
- Type:cves
- ID:CVE-2019-16163 CVE-2019-19204 CVE-2019-16255 CVE-2019-19246
- SUG:N/A
- DESC:fix CVE-2019-16163CVE-2019-19204CVE-2019-16255CVE-2019-19246

* Mon Feb 03 2020 Yiru Wang <wangyiru1@huawei.com> - 2.5.1-103
- Type:cves
- ID:CVE-2019-16254
- SUG:N/A
- DESC:fix CVE-2019-16254

* Thu Jan 16 2020 fengbing <fengbing7@huawei.com> - 2.5.1-102
- Type:N/A
- ID:N/A
- SUG:N/A
- DESC:modify source0 in spec file

* Mon Dec 30 2019 lihao openEuler Buildteam <buildteam@openeuler.org> - 2.5.1-101
- Type:N/A
- ID:N/A
- SUG:N/A
- DESC:modify info in patch

* Wed Dec 25 2019 lihao <lihao129@huawei.com> - 2.5.1-100
- Type:cves
- ID:CVE-2019-15845 CVE-2019-16201
- SUG:N/A
- DESC:fix CVE-2019-15845 CVE-2019-16201

* Sat Nov 30 2019 fengbing <fengbing7@huawei.com> - 2.5.1-99
- Package init
